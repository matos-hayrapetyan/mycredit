<?php

require 'core/autoload.php';

require 'core/MyCredit.php';

function MyCredit()
{
    return \MyCredit\MyCredit::instance();
}

$GLOBALS['mycredit'] = MyCredit();