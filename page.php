<?php
get_header();
?>
    <div class="entry">
        <div class="container">
            <div class="main-container">
                <div class="content post-content">
                    <?php the_content(); ?>
                </div><!-- .post-content -->
                <?php \MyCredit\Helpers\View::render('sidebar.view.php'); ?>
            </div><!-- .main-container -->
        </div><!-- .container -->
    </div><!-- .entry -->
<?php
get_footer();