<?php

namespace MyCredit\Controllers\Admin;


class MetaBoxesController
{

    public function __construct()
    {
        add_action('add_meta_boxes',array(__CLASS__,'addMetaBoxes'));
        add_action( 'save_post', array( __CLASS__, 'save' ) );
    }

    public static function addMetaBoxes()
    {
        add_meta_box(
            'mycredit_service',
            'Service(Service Related Data)',
            array( __CLASS__, 'metaBox' ),
            'post',
            'normal',
            'high'
        );
    }

    public static function metaBox()
    {
        global $post;
        $icon = get_post_meta($post->ID, 'mycredit_service_icon', true);
        $hover_icon = get_post_meta($post->ID, 'mycredit_service_hover_icon', true);
        ?>
        <div id="mycredit_service">
            <div>
                <label>
                    <span>Service Icon(SVG code)</span>
                    <textarea style="width: 100%;" name="mycredit_service_icon"><?=$icon; ?></textarea>
                </label>
            </div>
            <label>
                <span>Service Hover Icon(SVG code)</span>
                <textarea style="width: 100%;" name="mycredit_service_hover_icon"><?=$hover_icon; ?></textarea>
            </label>
        </div>
        <?php
    }

    /**
     * Save meta data
     */
    public static function save(){
        global $post;

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

        if( !isset( $post ) || wp_is_post_revision( $post->ID ) || !isset( $_POST['mycredit_service_icon'] )){
            return;
        }

        update_post_meta( $post->ID, 'mycredit_service_icon', $_POST['mycredit_service_icon'] );
        update_post_meta( $post->ID, 'mycredit_service_hover_icon',$_POST['mycredit_service_hover_icon'] );
    }
    
}