<?php

namespace MyCredit\Controllers\Admin;

/**
 * Class AdminAssetsController
 * @package MyCredit\Controllers\Admin
 */
class AdminAssetsController
{
    /**
     * AdminAssetsController constructor.
     */
    public function __construct()
    {
        add_action('admin_enqueue_scripts',array(__CLASS__,'scripts'));
    }


    public static function scripts()
    {
        wp_enqueue_script('mycredit-inline-popup', MYCREDIT_ASSETS_URL.'/js/admin/inline-popup.js');
    }

}