<?php

namespace MyCredit\Controllers\Admin;

class AdminController
{

    public function __construct()
    {
        new MetaBoxesController();
        new AdminAssetsController();
        add_action( 'admin_footer', array( '\MyCredit\Controllers\Admin\ShortcodeController', 'showInlinePopup' ) );
        add_action( 'media_buttons_context', array( '\MyCredit\Controllers\Admin\ShortcodeController', 'showEditorMediaButton' ) );
    }

}