<?php

namespace MyCredit\Controllers\Admin;

use MyCredit\Helpers\View;

class ShortcodeController
{

    public static function showInlinePopup()
    {
        View::render( 'admin/inline-popup.view.php' );
    }

    public static function showEditorMediaButton($context)
    {
        $container_id = 'mycredit_pdf';
        $title        = __( 'Select PDF File to insert into post', MYCREDIT_TEXTDOMAIN );
        $button_text  = __( 'PDF File', MYCREDIT_TEXTDOMAIN );
        $context .= '<a class="button thickbox" title="' . $title . '"    href="#TB_inline?width=400&inlineId=' . $container_id . '">'. $button_text .'</a>';

        return $context;
    }

}