<?php

namespace MyCredit\Controllers\Frontend;

/**
 * Class FrontendController
 * @package MyCredit\Controllers\Frontend
 */
class FrontendController
{
    public function __construct()
    {
        add_shortcode( 'mycredit_pdf', array( 'MyCredit\Controllers\Frontend\ShortcodeController', 'run' ) );
        new FrontendAssetsController();
        //shortcodes for pdf
    }
}