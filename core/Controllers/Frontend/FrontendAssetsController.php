<?php

namespace MyCredit\Controllers\Frontend;


class FrontendAssetsController
{

    public function __construct()
    {
        add_action('wp_enqueue_scripts', array(__CLASS__, 'scripts'));
        add_action('wp_enqueue_scripts', array(__CLASS__, 'styles'));
    }

    public static function scripts()
    {
        wp_enqueue_script('mycredit-slider', MYCREDIT_ASSETS_URL.'/js/jquery.slider.js', array('jquery'), \MyCredit()->version, true);
        wp_enqueue_script('mycredit-modal', MYCREDIT_ASSETS_URL.'/js/mycredit-modal.js', array('jquery'), \MyCredit()->version, true);
        wp_enqueue_script('mycredit-scripts', MYCREDIT_ASSETS_URL.'/js/scripts.js', array('mycredit-slider'), \MyCredit()->version, true);
        wp_localize_script('mycredit-scripts', 'myCreditL10n', array(
            'ajaxUrl' => admin_url('admin-ajax.php'),
            'months' => __('months',MYCREDIT_TEXTDOMAIN),
            'month' => __('Month',MYCREDIT_TEXTDOMAIN),
            'calculationResults' => __('Calculation Results',MYCREDIT_TEXTDOMAIN),
            'remainder' => __('Remainder',MYCREDIT_TEXTDOMAIN),
            'interest' => __('Interest',MYCREDIT_TEXTDOMAIN),
            'principalAmount' => __('Principal amount',MYCREDIT_TEXTDOMAIN),
            'payment' => __('Payment',MYCREDIT_TEXTDOMAIN),
            'error' => __('Please fill all the fields',MYCREDIT_TEXTDOMAIN),
            'chooseLoanType' => __('Please select the loan type',MYCREDIT_TEXTDOMAIN),
            'formSuccess' => __('Thank You! Your message has been sent',MYCREDIT_TEXTDOMAIN),
            'viewMore' => __('View More',MYCREDIT_TEXTDOMAIN),
            'viewLess' => __('View Less',MYCREDIT_TEXTDOMAIN),
            'emailError' => __('Email field is required',MYCREDIT_TEXTDOMAIN),
            'contactSuccess' =>
                sprintf(
                    '<span class="big">%s</span>%s',
                    __('Thank You!',MYCREDIT_TEXTDOMAIN),
                    __('Your message has been sent.',MYCREDIT_TEXTDOMAIN)
                ),
        ));
    }

    public static function styles()
    {
        wp_enqueue_style('mycredit-styles', MYCREDIT_ASSETS_URL.'/css/styles.css');
        wp_enqueue_style('mycredit-jslider', MYCREDIT_ASSETS_URL.'/css/jquery.jslider-all.css');
        wp_enqueue_style('mycredit-modal', MYCREDIT_ASSETS_URL.'/css/mycredit-modal.css');
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500');
    }


}