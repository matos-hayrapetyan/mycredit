<?php

namespace MyCredit\Controllers\Frontend;


use MyCredit\Helpers\View;
use MyCredit\Services\Blog;

class AjaxController
{

    public static function init()
    {
        add_action('wp_ajax_mycredit_apply_online',array(__CLASS__, 'applyOnline'));
        add_action('wp_ajax_nopriv_mycredit_apply_online',array(__CLASS__, 'applyOnline'));


        add_action('wp_ajax_mycredit_blog_posts',array(__CLASS__, 'blogPosts'));
        add_action('wp_ajax_nopriv_mycredit_blog_posts',array(__CLASS__, 'blogPosts'));


        add_action('wp_ajax_mycredit_contact', array(__CLASS__, 'contact'));
        add_action('wp_ajax_nopriv_mycredit_contact', array(__CLASS__, 'contact'));
    }

    public static function contact()
    {
        if(!isset($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'],'mycredit_contact')){
            die(0);
        }

        $name = sanitize_text_field($_GET['contact-name']);
        $subject = sanitize_text_field($_GET['contact-subject']);
        $email = sanitize_email($_GET['contact-email']);
        $message = sanitize_text_field($_GET['contact-message']);

        $to = get_option('admin_email');

        $headers[0] = 'From: ' . $name . ' <no-reply@huge-it.com>';
        $headers[1] = 'Reply-To: '.$email.' ';
        $headers[2] = 'Content-Type: text/html; charset=UTF-8';


        if(wp_mail($to,$subject,$message,$headers)){
            echo 'ok';
            die;
        }else{
            echo 'ok';
            die;
        }
    }

    public static function applyOnline()
    {
        if(!isset($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'],'mycredit_apply_online')){
            die(0);
        }

        $loanType = sanitize_text_field($_POST['loan-type']);
        $amount = sanitize_text_field($_POST['desired-amount']);
        $name = sanitize_text_field($_POST['select-name']);
        $surname = sanitize_text_field($_POST['select-surname']);
        $income = sanitize_text_field($_POST['select-income']);
        $phone = sanitize_text_field($_POST['select-phone']);
        $customerEmail = sanitize_text_field($_POST['select-email']);

        $subject = 'Website | Online Application Form';

        $headers[0] = 'From: ' . $name . ' <no-reply@mycredit.am>';
        $headers[1] = 'Reply-To: '.$customerEmail.' ';
        $headers[2] = 'Content-Type: text/html; charset=UTF-8';

        $to = get_option('admin_email');

        $message = "<strong>Loan Type</strong>: $loanType<br/>\r\n";
        $message .= "<strong>Desired amount of loan</strong>: $amount<br/>\r\n";
        $message .= "<strong>Name</strong>: $name<br/>\r\n";
        $message .= "<strong>Surname</strong>: $surname<br/>\r\n";
        $message .= "<strong>Average Monthly Income</strong>: $income<br/>\r\n";
        $message .= "<strong>Phone Number</strong>: $phone<br/>\r\n";
        $message .= "<strong>Email</strong>: $customerEmail<br/>\r\n";


        if(wp_mail($to,$subject,$message,$headers)){
            echo 'ok';
            die;
        }else{
            echo 'mail rejected by server';
            die;
        }
    }

    public static function blogPosts()
    {
        if(!isset($_GET['paged'])){
            $paged = 1;
        }else{
            $paged = absint($_GET['paged']);
        }

        $posts = Blog::getBlogPosts(array(
            'paged' => $paged,
            'posts_per_page' => 4
        ));

        View::render('archive/blog-posts.view.php', compact('posts'));

    }

}