<?php

namespace MyCredit\Controllers\Frontend;


use MyCredit\Helpers\View;

class ShortcodeController
{

    public static function run($attrs)
    {
        $attrs = shortcode_atts(array(
            'src' => false,
            'title' => '',
            'type' => 'white',
        ), $attrs);

        if (!$attrs['src'] || absint($attrs['src']) != $attrs['src']) {
            throw new \Exception('"id" parameter is required and must be not negative integer.');
        }


        do_action('mycredit_pdf_shortcode', $attrs['src']);

        return View::buffer('shortcodes/pdf.view.php', array(
                'src' => $attrs['src'],
                'title' => $attrs['title'],
                'type' => $attrs['type'],
            )
        );
    }

}