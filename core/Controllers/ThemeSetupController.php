<?php

namespace MyCredit\Controllers;

class ThemeSetupController
{

    public static function run()
    {
        global $content_width;
        $template_directory = get_template_directory();
        load_theme_textdomain( 'mycredit', $template_directory . '/languages' );
        if ( ! isset( $content_width ) ) {
            $content_width = 2500;
        }
        register_nav_menus( array(
            'primary' => __( 'Navigation Menu', MYCREDIT_TEXTDOMAIN ),
        ) );

        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'widgets'
        ) );
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );

        add_image_size( 'blog-thumbnail', 390, 250 );
        set_post_thumbnail_size( 1200, 1200, true );

        register_nav_menu( 'primary', __( 'Primary Menu', 'grand_wp' ) );

        remove_filter( 'the_content', 'wpautop' );
        remove_filter( 'the_excerpt', 'wpautop' );
    }

}