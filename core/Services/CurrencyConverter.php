<?php

namespace MyCredit\Services;


class CurrencyConverter
{
    public static function init()
    {
        add_action('mycredit_daily_rates',array(__CLASS__,'runCronJob'));
    }

    public static function runCronJob()
    {
        ini_set('soap.wsdl_cache_enabled', '0');
        ini_set('soap.wsdl_cache_ttl', '0');

        define('WSDL', 'http://api.cba.am/exchangerates.asmx?wsdl');

        $error = false;

        try {
            $client = new \SoapClient(WSDL, array(
                'version' => SOAP_1_1
            ));
            $result = $client->__soapCall("ExchangeRatesByDate", array(array(
                'date' => date('Y-m-d\TH:i:s')
            )));
            if (is_soap_fault($result)) {
                throw new \Exception('Failed to get data');
            } else {
                $data = $result->ExchangeRatesByDateResult;
            }
        } catch (\Exception $e) {
            error_log(';Message: ' . $e->getMessage() . "\nTrace:" . $e->getTraceAsString());
        }

        if ($error === false) {

            $rates = $data->Rates->ExchangeRate;

            if (is_array($rates) && count($rates) > 0) {

                global $wpdb;

                foreach ($rates as $rate) {
                    $wpdb->query(
                        $wpdb->prepare(
                            'INSERT INTO `'.$wpdb->prefix.'MyCreditCurrencies` 
                            (`iso_code`,`amount`,`rate`,`difference`,`updated_at`) 
                            VALUES (%s,%s,%s,%s,NOW()) 
                            ON DUPLICATE KEY UPDATE 
                            `amount`=%s,
                            `rate`=%s,
                            `difference`=%s,
                            `updated_at`=NOW()'
                            ,
                            $rate->ISO,
                            $rate->Amount,
                            $rate->Rate,
                            $rate->Difference,
                            $rate->Amount,
                            $rate->Rate,
                            $rate->Difference
                        )
                    );
                }

            }
        }
    }

}