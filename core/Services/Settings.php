<?php

namespace MyCredit\Services;

use MyCredit\Vendor\WPDevSettingsApi\WPDevSettingsApi;

class Settings extends WPDevSettingsApi
{

    public $plugin_id = '_mycredit';
    public $services_cat;

    public $usd_buy_rate;
    public $usd_sell_rate;

    public $eur_buy_rate;
    public $eur_sell_rate;

    public $rub_buy_rate;
    public $rub_sell_rate;

    /**
     * HG_Login_Settings constructor.
     */
    public function __construct(){
        $config = array(
            'menu_slug' => 'mycredit_settings',
            'page_title' => __( 'MyCredit Settings', 'hg_login' ),
            'title' => __('MyCredit Setting','hg_login'),
            'menu_title'=> __( 'MyCredit', 'hg_login' ),
            'subtitle' => __( 'Settings', 'hg_login' )
        );

        $this->init();
        $this->init_panels();
        $this->init_sections();
        $this->init_controls();

        parent::__construct( $config);

    }

    private function init(){
        $this->services_cat = $this->get_option( 'services_cat', '' );
        $this->usd_buy_rate = $this->get_option( 'usd_buy_rate', '480' );
        $this->usd_sell_rate = $this->get_option( 'usd_sell_rate', '484.50' );
        $this->eur_buy_rate = $this->get_option( 'eur_buy_rate', '535' );
        $this->eur_sell_rate = $this->get_option( 'eur_sell_rate', '546' );
        $this->rub_buy_rate = $this->get_option( 'rub_buy_rate', '8.34' );
        $this->rub_sell_rate = $this->get_option( 'rub_sell_rate', '8.65' );
    }

    private function init_panels(){
        $this->panels = array(
            'global' => array(
                'title' => __( 'Global', MYCREDIT_TEXTDOMAIN )
            ),
        );
    }

    private function init_sections(){
        $this->sections = array(
            'categories' => array(
                'title' => __( 'Categories', MYCREDIT_TEXTDOMAIN ),
                'panel' => 'global'
            ),
            'rates' => array(
                'title' => __('Currency Rates for loan payments', MYCREDIT_TEXTDOMAIN),
                'panel' => 'global',
            )
        );
    }

    private function init_controls(){
        $this->controls = array(
            'services_cat' => array(
                'section' => 'categories',
                'type' => 'category_dropdown',
                'default' => $this->services_cat,
                'label' => __( 'Services Category', MYCREDIT_TEXTDOMAIN )
            ),
            'usd_buy_rate' => array(
                'section' => 'rates',
                'type' => 'text',
                'default' => $this->usd_buy_rate,
                'label' => __('USD - BUY rate',MYCREDIT_TEXTDOMAIN),
            ),
            'usd_sell_rate' => array(
                'section' => 'rates',
                'type' => 'text',
                'default' => $this->usd_sell_rate,
                'label' => __('USD - SELL rate',MYCREDIT_TEXTDOMAIN),
            ),
            'eur_buy_rate' => array(
                'section' => 'rates',
                'type' => 'text',
                'default' => $this->eur_buy_rate,
                'label' => __('EUR - BUY rate',MYCREDIT_TEXTDOMAIN),
            ),
            'eur_sell_rate' => array(
                'section' => 'rates',
                'type' => 'text',
                'default' => $this->eur_sell_rate,
                'label' => __('EUR - SELL rate',MYCREDIT_TEXTDOMAIN),
            ),
            'rub_buy_rate' => array(
                'section' => 'rates',
                'type' => 'text',
                'default' => $this->rub_buy_rate,
                'label' => __('RUB - BUY rate',MYCREDIT_TEXTDOMAIN),
            ),
            'rub_sell_rate' => array(
                'section' => 'rates',
                'type' => 'text',
                'default' => $this->rub_sell_rate,
                'label' => __('RUB - SELL rate',MYCREDIT_TEXTDOMAIN),
            ),
        );
    }

}