<?php

namespace MyCredit\Services;


use MyCredit\Models\ServicePost;

class Blog
{

    /**
     * @return ServicePost[]
     */
    public static function getServicePosts()
    {
        $args = array(
            'tag' => 'service',
            'cat' => \MyCredit()->settings->services_cat,
        );

        $query = new \WP_Query($args);

        if(!$query->have_posts()){
            return null;
        }

        $posts = $query->get_posts();

        $res = [];
        foreach($posts as $post){
            $res[] = new ServicePost($post);
        }

        return $res;
    }

    /**
     * @param array $args
     * @return \WP_Post[]
     */
    public static function getBlogPosts($args=array())
    {
        $args = wp_parse_args($args,array(
            'tag' => 'blog',
            'posts_per_page' => 4,
        ));

        $query = new \WP_Query($args);

        if(!$query->have_posts()){
            return null;
        }

        return  $query->get_posts();
    }

    public static function getBlogPostsPageCount($args=array())
    {
        $args = wp_parse_args($args,array(
            'tag' => 'blog',
            'posts_per_page' => 4,
        ));

        $query = new \WP_Query($args);
        if(!$query->have_posts()){
            return 0;
        }


        return $query->max_num_pages;
    }

}