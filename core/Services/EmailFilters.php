<?php
namespace MyCredit\Services;

class EmailFilters
{

    /**
     * Email Address that will be set as sender for emails
     * @var string
     */
    private $mail;
    /**
     * Name that will be set as sender for emails
     * @var string
     */
    private $name;
    /**
     * Grand_WP_Email_Filters constructor.
     * @param string $mail
     * @param string $name
     * @throws \Exception
     */
    public function __construct( $mail = null, $name = null ) {
        if( empty( $mail ) || empty( $name ) ){
            throw new \Exception( 'Properties "email" and "name" cannot be empty for Grand_WP_Email_Filters class' );
        }

        $this->mail = $mail;
        $this->name = $name;

        add_filter( 'wp_mail_from_name', array( $this, 'wp_mail_from_name' ) );
        add_filter('wp_mail_from', array( $this, 'wp_mail_from' ) );
    }
    /**
     * Change the default email address from which emails are sent
     */
    public function wp_mail_from( $mail ){
        if( $mail !== 'wordpress@mycredit.am' ){
            return $mail;
        }
        $mail = $this->mail;
        return $mail;
    }
    /**
     * Change the default name address from which emails are sent
     */
    public function wp_mail_from_name( $name ){
        if( $name !== 'WordPress' ){
            return $name;
        }
        $name = $this->name;
        return $name;
    }

}