<?php

namespace MyCredit\Database\Migrations;

class CreateCurrenciesTable
{

    public static function run()
    {
        global $wpdb;

        $wpdb->query('CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'MyCreditCurrencies`(
            `iso_code` VARCHAR(3) NOT NULL,
            `amount` VARCHAR(20) NOT NULL,
            `rate` FLOAT(20) NOT NULL,
            `difference` FLOAT(20) NOT NULL,
             `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
             PRIMARY KEY(iso_code)
        )');

    }

}