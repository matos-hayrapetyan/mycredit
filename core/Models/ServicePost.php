<?php

namespace MyCredit\Models;

class ServicePost
{

    /**
     * @var \WP_Post
     */
    private $post_data;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $hover_icon;

    public function __construct($post)
    {
        if ($post instanceof \WP_Post) {
            $this->post_data = $post;
            $this->id = $post->ID;
        } elseif (is_numeric($post)) {
            $this->post_data = get_post($post);
            $this->id = $post;
        } else {
            throw new \Exception('POST is REQUIRED for ServicePost');
        }

        $this->icon = get_post_meta($this->id, 'mycredit_service_icon', true);
        $this->hover_icon = get_post_meta($this->id, 'mycredit_service_hover_icon', true);

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return ServicePost
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return string
     */
    public function getHoverIcon()
    {
        return $this->hover_icon;
    }

    /**
     * @param string $hover_icon
     * @return ServicePost
     */
    public function setHoverIcon($hover_icon)
    {
        $this->hover_icon = $hover_icon;
        return $this;
    }

    public function getPostData()
    {
        return $this->post_data;
    }

    public function getPermalink()
    {
        return get_the_permalink($this->id);
    }
}