<?php

namespace MyCredit\Models;

/**
 * Class CurrencyRates
 * @package MyCredit\Models
 */
class CurrencyRates
{

    /**
     * @var string
     */
    private static $table_name = 'MyCreditCurrencies';

    /**
     * @var array
     */
    private $rates = array();

    /**
     * CurrencyRates constructor.
     */
    public function __construct()
    {

        global $wpdb;

        $results = $wpdb->get_results("SELECT * FROM ".self::getTableName()." WHERE `iso_code` IN ('USD','EUR','RUB','GBP','GEL','CHF')", ARRAY_A);

        if(!empty($results)){
            foreach($results as $r){
                $this->rates[$r['iso_code']] = array(
                    'amount' => $r['amount'],
                    'rate' => $r['rate'],
                    'difference' => ($r['difference']>0 ? '+'.$r['difference'] : $r['difference']),
                    'updated_at' => $r['updated_at']
                );
            }
        }
        
    }

    /**
     * @return string
     */
    public static function getTableName()
    {
        return $GLOBALS['wpdb']->prefix.self::$table_name;
    }

    /**
     * @return array
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @param $iso_code
     * @return array
     */
    public function getRate($iso_code)
    {
        if(isset($this->rates[$iso_code])){
            return $this->rates[$iso_code];
        }

        return null;
    }

}