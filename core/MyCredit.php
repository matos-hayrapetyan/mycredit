<?php

namespace MyCredit;

use MyCredit\Controllers\Frontend\AjaxController as FrontAjax;
use MyCredit\Controllers\Admin\AdminController;
use MyCredit\Controllers\Frontend\FrontendController;
use MyCredit\Services\CurrencyConverter;
use MyCredit\Services\Settings;
use MyCredit\Services\EmailFilters;

class MyCredit
{

    /**
     * version of website
     * @var string
     */
    public $version = '1.0.0';

    /**
     * @var Settings
     */
    public $settings;

    /**
     * @var AdminController
     */
    public $admin;

    /**
     * Class names of migration classes
     *
     * @var array
     */
    private $MigrationClasses;

    /**
     * @var MyCredit
     */
    protected static $_instance = null;

    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct()
    {
        if (!is_null(self::$_instance)) {
            throw new \Exception('trying to create duplicate object.');
        }

        $this->MigrationClasses = array(
            '\MyCredit\Database\Migrations\CreateCurrenciesTable',
        );

        $this->initHooks();
        $this->defineConstants();
    }

    private function initHooks()
    {
        add_action('init', array($this, 'init'), 0);
        add_action( 'after_setup_theme', array( 'MyCredit\Controllers\ThemeSetupController', 'run' ) );

        add_action('init',array($this,'scheduleRates'),0);
        add_filter('cron_schedules',array($this,'customCronJobRecurrence'));
    }

    private function defineConstants()
    {
        define('MYCREDIT_TEXTDOMAIN', 'mycredit');
        define('MYCREDIT_ASSETS_URL', $this->themeUrl().'/core/resources/assets');
        define('MYCREDIT_ASSETS_PATH', $this->themePath().DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'assets');
    }

    public function init()
    {

        $this->checkVersion();

        if(defined('DOING_CRON')){

            CurrencyConverter::init();
        }

        $this->settings = new Settings();

        if(defined('DOING_AJAX')){
            FrontAjax::init();
        }

        new EmailFilters('no-reply@mycredit.am', 'MyCredit');

        if(is_admin()){
            $this->admin = new AdminController();
        }else{
            new FrontendController();
        }
    }

    private function checkVersion(){
        if (get_option('mycredit_version') !== $this->version) {
            $this->runMigrations();
            update_option('mycredit_version', $this->version);
        }
    }

    private function runMigrations(){
        if (empty($this->MigrationClasses)) {
            throw new \Exception('migrations empty');
        }

        foreach ($this->MigrationClasses as $className) {
            if (method_exists($className, 'run')) {
                call_user_func(array($className, 'run'));
            } else {
                throw new \Exception('Specified migration class ' . $className . ' does not have "run" method');
            }
        }
    }

    /* cron job custom schedule */
    public function scheduleRates()
    {
        if ( ! wp_next_scheduled( 'mycredit_daily_rates' ) ) {
            CurrencyConverter::runCronJob();
            wp_schedule_event( current_time( 'timestamp' ), 'mycredit_daily', 'mycredit_daily_rates' );
        }
    }

    /* add daily cron schedule name */
    public function customCronJobRecurrence($schedules)
    {
        $schedules['mycredit_daily'] = array(
            'display' => __( 'Daily', MYCREDIT_TEXTDOMAIN ),
            'interval' => 86400,
        );
        return $schedules;
    }

    public function templatePath()
    {
        return apply_filters('mycredit_template_path', 'core/resources/views');
    }

    /**
     * Get the plugin url.
     * @return string
     */
    public function themeUrl()
    {
        return untrailingslashit(get_template_directory_uri());
    }

    /**
     * Get the plugin path.
     * @return string
     */
    public function themePath()
    {
        return untrailingslashit(get_template_directory());
    }

}