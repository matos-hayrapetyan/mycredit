<?php

namespace MyCredit\Helpers;

class View
{

    /**
     * @param $template_name
     * @param string $template_path
     * @param string $default_path
     *
     * @return mixed
     */
    private static function locate($template_name, $template_path = '', $default_path = '' ) {
        if ( ! $template_path ) {
            $template_path = \MyCredit()->templatePath();
        }
        if ( ! $default_path ) {
            $default_path = \MyCredit()->themePath() . '/core/resources/views/';
        }
        /**
         * Look within passed path within the theme - this is priority.
         */
        $template = locate_template(
            array(
                $template_name,
                trailingslashit( $template_path ) . $template_name,
            )
        );
        /**
         * Get default template
         */
        if ( ! $template ) {
            $template = $default_path . $template_name;
        }

        /**
         * Return what we found.
         */
        return apply_filters( 'mycredit_locate_template', $template, $template_name, $template_path );
    }

    /**
     * @param $templateName
     * @param array $args
     * @param string $templatePath
     * @param string $defaultPath
     */
    public static function render($templateName, $args = array(), $templatePath = '', $defaultPath = '' ) {
        if ( $args && is_array( $args ) ) {

            extract( $args );

        }

        $located = self::locate( $templateName, $templatePath, $defaultPath );

        if ( ! file_exists( $located ) ) {

            _doing_it_wrong( __FUNCTION__, sprintf( '<code>%s</code> does not exist.', $located ), '1.0' );

            return;

        }

        // Allow 3rd party plugin filter template file from their plugin.
        $located = apply_filters( 'mycredit_get_template', $located, $templateName, $args, $templatePath, $defaultPath );

        do_action( 'mycredit_before_template_part', $templateName, $templatePath, $located, $args );

        include( $located );

        do_action( 'mycredit_after_template_part', $templateName, $templatePath, $located, $args );
    }

    /**
     * @param $template_name
     * @param array $args
     * @param string $template_path
     * @param string $default_path
     * @return string
     */
    public static function buffer($template_name, $args = array(), $template_path = '', $default_path = '')
    {
        ob_start();
        self::render($template_name, $args, $template_path, $default_path);
        return ob_get_clean();
    }

}