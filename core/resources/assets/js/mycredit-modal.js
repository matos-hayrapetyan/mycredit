var myCreditModal = {
    show: function(elementId, args){
        var el = jQuery('#'+elementId);
        if(el.length){
            el.css('display','flex');
        }
    },

    hide: function(elementId){
        var el = jQuery('#'+elementId);
        el.css('display','none');
    }
};

jQuery(document).ready(function(){
    jQuery('body').on('click','.-mycredit-modal-close',function(){
        myCreditModal.hide(jQuery(this).closest('.-mycredit-modal').attr('id'));
    });
});