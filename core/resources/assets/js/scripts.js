jQuery(document).ready(function(){
    var applyingForm = false;


    jQuery("#calculator-amount").slider({
        from: 500,
        to: 20000,
        step: 100,
        round: 1,
        format: { format: '#,###', locale: 'us' },
        dimension: ''
    });

    jQuery("#calculator-duration").slider({
        from: 12,
        to: 120,
        step: 1,
        round: 1,
        format: { format: '#', locale: 'us' },
        dimension: '&nbsp;'+myCreditL10n.months
    });

    var contacted = false;
    jQuery('#contact_form').on('submit',function(e){
        e.preventDefault();
        if(contacted) return false;
        var email = jQuery('#contact-email').val(),
            thisForm = jQuery(this),
            submitBtn = thisForm.find('button[type="submit"]'),
            originalText = submitBtn.html();


        var emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!emailRegexp.test(email)){
            alert(myCreditL10n.emailError);
            return false;
        }


        jQuery.ajax({
            type: "POST",
            url: myCreditL10n.ajaxUrl,
            data: thisForm.serialize(),
            beforeSend: function(){
                applyingForm = true;
                submitBtn.attr('disabled','disabled');
                var spinnerHtml = '<div class="-spinner">' +
                    '<div class="-spin -spinner-1"></div>' +
                    '<div class="-spin -spinner-2"></div>' +
                    '<div class="-spin -spinner-3"></div>' +
                    '<div class="-spin -spinner-4"></div>' +
                    '</div>';
                submitBtn.html(spinnerHtml);
            }
        }).always(function (data, textStatus, errorThrown) {
            applyingForm = false;
            submitBtn.removeAttr('disabled');
        }).done(function (data, textStatus, jqXHR) {
            if(data === 'ok'){
                contacted = true;
                submitBtn.replaceWith('<p class="contact-success">'+myCreditL10n.contactSuccess+'</p>');
            }else{
                submitBtn.html(originalText);
            }
        });

        return false;
    });

    jQuery("#apply-online").on('submit',function(e){
        e.preventDefault();
        if(applyingForm) return false;
        var loanType = jQuery('#loan-type').val(),
            thisForm = jQuery(this),
            submitBtn = thisForm.find('button[type="submit"]'),
            originalText = submitBtn.html();

        if(!loanType){
            alert(myCreditL10n.chooseLoanType);
            return false;
        }

        jQuery.ajax({
            type: "POST",
            url: myCreditL10n.ajaxUrl,
            data: thisForm.serialize(),
            beforeSend: function(){
                applyingForm = true;
                submitBtn.attr('disabled','disabled');
                var spinnerHtml = '<div class="-spinner">' +
                    '<div class="-spin -spinner-1"></div>' +
                    '<div class="-spin -spinner-2"></div>' +
                    '<div class="-spin -spinner-3"></div>' +
                    '<div class="-spin -spinner-4"></div>' +
                    '</div>';
                submitBtn.html(spinnerHtml);
            }
        }).always(function (data, textStatus, errorThrown) {
            applyingForm = false;
            submitBtn.removeAttr('disabled');
        }).done(function (data, textStatus, jqXHR) {
            if(data === 'ok'){
                submitBtn.replaceWith('<p class="-form-success">'+myCreditL10n.formSuccess+'</p>')
            }else{
                submitBtn.html(originalText);
            }
        });

        return false;
    });

    jQuery('#calculation-form').on('submit',function(e){
        e.preventDefault();
        var template='<div id="mycredit-calculation-modal" class="-mycredit-modal text-center"><div class="-mycredit-modal-content"><div class="-mycredit-modal-content-header"><div class="-mycredit-modal-content-title">'+myCreditL10n.calculationResults+'</div><div class="-mycredit-modal-close"></div></div><div class="-mycredit-modal-content-body"><table>{CONTENT}</table></div></div></div>';
        var content='<tr><th>'+myCreditL10n.month+'</th><th>'+myCreditL10n.remainder+'</th><th>'+myCreditL10n.interest+'</th><th>'+myCreditL10n.principalAmount+'</th><th>'+myCreditL10n.payment+'</th></tr>';

        var amount=parseInt("0"+jQuery("#calculator-amount").val(),10)*1000;
        var duration=parseInt("0"+jQuery("#calculator-duration").val(),10); //months
        var percent=parseInt("0"+jQuery("#calculator-percent").val(),10);
        var method=jQuery("#calculator-method").val(); //monthlyfixed, decreasing

        var error=myCreditL10n.error;

        if(amount==0){
            amount=500000;
        }

        if(duration==0){
            alert(error);
            return false;
        }

        if(percent==0){
            jQuery("#calculator-percent").get(0).focus();
            alert(error);
            return false;
        }



        if(method=="monthlyfixed"){
            var monthlypercent=percent/12;
            var factor = Math.pow(1 + percent/1200, duration );
            var payment = amount*(percent/1200/(1 - 1/factor));

            var mnacord=amount;


            for(var i=1;i<=duration;i++){
                var tokos=mnacord/100*monthlypercent;
                var mayr=payment-tokos;
                mnacord=mnacord-mayr;
                if(i%2){var mod="even";}else{var mod="odd";}
                content+='<tr class="'+mod+'"><td>'+i+'</td><td>'+format_money(mnacord)+'</td><td>'+format_money(tokos)+'</td><td>'+format_money(mayr)+'</td><td>'+format_money(payment)+'</td></tr>';
            }
        }
        else if(method=="decreasing"){
            //amount, percent

            var mayr=amount/duration;
            var year_days=365;
            var mnacord=amount;

            for(var i=1;i<=duration;i++){
                var tokos=mnacord/100*percent/year_days*30;
                var payment=mayr+tokos;
                mnacord=mnacord-mayr;
                if(i%2){var mod="even";}else{var mod="odd";}
                content+='<tr class="'+mod+'"><td>'+i+'</td><td>'+format_money(mnacord)+'</td><td>'+format_money(tokos)+'</td><td>'+format_money(mayr)+'</td><td>'+format_money(payment)+'</td></tr>';
            }
        }
        else{
            alert(error);
            return false;
        }

        template=template.replace(/{CONTENT}/g, content);

        if(jQuery("#mycredit-calculation-modal").length){
            jQuery("#mycredit-calculation-modal").remove();
        }
        jQuery("body").append(template);
        myCreditModal.show('mycredit-calculation-modal');

        return false;
    });


    if(jQuery('.slides li').length){
        var slidesInterval = setInterval(function(){
            myCreditNextSlide();
        },5000);
        jQuery('.dots').on('mouseenter',function(){
            clearInterval(slidesInterval);
        });

        jQuery('.dots').on('mouseleave',function(){
            slidesInterval = setInterval(function(){
                myCreditNextSlide();
            },5000);
        });

        jQuery('.dots li').on('click',function(){
             var slides = jQuery('.slides'),
                 index = jQuery(this).index();
             if(index == jQuery('.slides li.active').index()) return;

            jQuery('.slides li.active').removeClass('active');
            jQuery('.dots li.active').removeClass('active');
            slides.find('li').eq(index).addClass('active');
            jQuery('.dots').find('li').eq(index).addClass('active');
        });
    }

    jQuery('.bio-tabs .tab-nav li').on('click',function(){
        var index = jQuery(this).index(),
            activeIndex = jQuery(this).parent().find('li.active');
        if(activeIndex == index) return false;

        jQuery('.bio-tabs .tab-nav li.active').removeClass('active');
        jQuery('.bio-tabs .tab-content li.active').removeClass('active');
        jQuery('.bio-tabs .tab-nav li').eq(index).addClass('active');
        jQuery('.bio-tabs .tab-content li').eq(index).addClass('active');
        return false;
    });

    jQuery('.job-positions .learn-more').on('click',function(){
        var position = jQuery(this).closest('.position');

        if(position.hasClass('active')){
            position.removeClass('active');
            jQuery(this).html(myCreditL10n.viewMore);
        }else{
            position.addClass('active');
            jQuery(this).html(myCreditL10n.viewLess);
        }

    });

    jQuery(document).ready(function(){
        if(jQuery('.blog-posts').length){
            doInfiniteScroll();
        }


        function doInfiniteScroll(){
            var flag = false,
                list = jQuery('.blog-posts'),
                total = + list.data('total');
            jQuery(window).scroll(function(){
                if(flag)
                    return; //return if the infinite pagination is calling
                if  (jQuery(window).scrollTop() > (jQuery(document).height() - jQuery(window).height())*90/100){
                    flag = true;  //set flag
                    infiniteLoad();
                }
            });

            function infiniteLoad(){
                var currentPage = list.data('paged'),
                    nextPage = + currentPage+1;
                if(currentPage >= total){
                    return;
                }
                jQuery.ajax({
                    url: myCreditL10n.ajaxUrl,
                    data: {
                        action: 'mycredit_blog_posts',
                        paged:nextPage
                    }
                }).done(function(data){
                    flag = false; //unset flag

                    list.append(data).data('paged',nextPage);
                })
                //do something
            }
        }
    });







});

function format_money(money){
    return jQuery.formatNumber(money, {format:"#,##0.00", locale:"us"});
}

function myCreditNextSlide(){
    var slides = jQuery('.slides'),
        index = jQuery('.slides li.active').index(),
        len = slides.find('li').length,
        next;
    if(index >= len -1 ){
        next = 0;
    }else{
        next = index+1;
    }

    jQuery('.slides li.active').removeClass('active');
    jQuery('.dots li.active').removeClass('active');
    slides.find('li').eq(next).addClass('active');
    jQuery('.dots').find('li').eq(next).addClass('active');
}