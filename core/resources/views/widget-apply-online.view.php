<div class="widget">
    <div class="widget-title"><?php _e('Apply Online   ',MYCREDIT_TEXTDOMAIN); ?></div>
    <div class="widget-content">
        <form id="apply-online" action="#" method="post">
            <input type="hidden" name="action" value="mycredit_apply_online" />
            <?php wp_nonce_field('mycredit_apply_online'); ?>
            <div class="-field-wrap">
                <div class="-field">
                    <select name="loan-type" id="loan-type" class="-field-text -field-full-width" title="Method" >
                        <option value=""><?php _e('Loan Type',MYCREDIT_TEXTDOMAIN); ?></option>
                        <option value="<?php _e('Car Loans', MYCREDIT_TEXTDOMAIN); ?>"><?php _e('Car Loans', MYCREDIT_TEXTDOMAIN); ?></option>
                        <option value="<?php _e('Mortgage Loans', MYCREDIT_TEXTDOMAIN); ?>"><?php _e('Mortgage Loans', MYCREDIT_TEXTDOMAIN); ?></option>
                        <option value="<?php _e('Consumer Loans', MYCREDIT_TEXTDOMAIN); ?>"><?php _e('Consumer Loans', MYCREDIT_TEXTDOMAIN); ?></option>
                        <option value="<?php _e('Lombard Loans', MYCREDIT_TEXTDOMAIN); ?>"><?php _e('Lombard Loans', MYCREDIT_TEXTDOMAIN); ?></option>
                        <option value="<?php _e('Business/ Agricultural Loans', MYCREDIT_TEXTDOMAIN); ?>"><?php _e('Business/ Agricultural Loans', MYCREDIT_TEXTDOMAIN); ?></option>
                    </select>
                </div>
            </div>
            <div class="-field-wrap">
                <div class="-field">
                    <input type="text" name="desired-amount" id="desired-amount" class="-field-text -field-text--material -field-full-width" title="Percent" required="required" />
                    <label for="desired-amount">Desired amount of loan</label>
                </div>
            </div>
            <div class="-field-wrap">
                <div class="-field">
                    <input type="text" name="select-name" id="select-name" class="-field-text -field-text--material -field-full-width" title="Name" required="required" />
                    <label for="select-name">Name</label>
                </div>
            </div>
            <div class="-field-wrap">
                <div class="-field">
                    <input type="text" name="select-surname" id="select-surname" class="-field-text -field-text--material -field-full-width" title="Surname" required="required" />
                    <label for="select-surname">Surname</label>
                </div>
            </div>
            <div class="-field-wrap">
                <div class="-field">
                    <input type="text" name="select-income" id="select-income" class="-field-text -field-text--material -field-full-width" title="Income" required="required" />
                    <label for="select-income">Income</label>
                </div>
            </div>
            <div class="-field-wrap">
                <div class="-field">
                    <input type="text" name="select-phone" id="select-phone" class="-field-text -field-text--material -field-full-width" title="Phone" required="required" />
                    <label for="select-phone">Phone</label>
                </div>
            </div>
            <div class="-field-wrap">
                <div class="-field">
                    <input type="text" name="select-email" id="select-email" class="-field-text -field-text--material -field-full-width" title="Email" required="required" />
                    <label for="select-email">Email</label>
                </div>
            </div>
            <div class="-field-wrap text-center">
                <button type="submit" class="btn bt--primary"><span class="btn-text"><?php _e('Apply now',MYCREDIT_TEXTDOMAIN); ?></span></button>
            </div>
        </form>
    </div>
</div>