<?php
/**
 * Show Home Page Content
 */
?>
<section id="home-banner">
    <ul class="slides">
        <li class="active">
            <div class="banner-img">
                <img src="<?=MYCREDIT_ASSETS_URL.'/images/home/slide-1.jpg'; ?>" alt="img" />
            </div>
            <div class="banner-content">
                <div class="banner-heading"><?php _e('Apply For Car Loan Online!',MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="banner-subheading"><?php _e('Lorem ipsum dolor sit amet, sea ne legimus percipit, ut vix dolorem consulatu. Ei mea tollit deserunt, ex per atomorum intellegebat, no vix habeo quidam percipit.',MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="btn-block">
                    <a href="#" class="btn btn--cta"><?php _e('Apply Now',MYCREDIT_TEXTDOMAIN); ?></a>
                </div>
            </div>
        </li>
        <li>
            <div class="img">
                <img src="<?=MYCREDIT_ASSETS_URL.'/images/home/slide-2.jpg'; ?>" alt="img" />
            </div>
            <div class="banner-content">
                <div class="heading"><?php _e('Apply For Car Loan Online!',MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="subheading"><?php _e('Lorem ipsum dolor sit amet, sea ne legimus percipit, ut vix dolorem consulatu. Ei mea tollit deserunt, ex per atomorum intellegebat, no vix habeo quidam percipit.',MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="btn-block">
                    <a href="#" class="btn btn--cta"><?php _e('Apply Now',MYCREDIT_TEXTDOMAIN); ?></a>
                </div>
            </div>
        </li>
        <li>
            <div class="img">
                <img src="<?=MYCREDIT_ASSETS_URL.'/images/home/slide-1.jpg'; ?>" alt="img" />
            </div>
            <div class="banner-content">
                <div class="heading"><?php _e('Apply For Car Loan Online!',MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="subheading"><?php _e('Lorem ipsum dolor sit amet, sea ne legimus percipit, ut vix dolorem consulatu. Ei mea tollit deserunt, ex per atomorum intellegebat, no vix habeo quidam percipit.',MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="btn-block">
                    <a href="#" class="btn btn--cta"><?php _e('Apply Now',MYCREDIT_TEXTDOMAIN); ?></a>
                </div>
            </div>
        </li>
    </ul>
    <ul class="dots">
        <li class="active"><span></span></li>
        <li><span></span></li>
        <li><span></span></li>
    </ul>
</section>
<?php \MyCredit\Helpers\View::render('services.view.php'); ?>
<div class="bg-gray entry">
    <div class="container">
        <div class="main-container">
            <div class="content">
                <?php echo do_shortcode('[mycredit_pdf type="white" src="http://www.diogenpro.com/uploads/4/6/8/8/4688084/danijel_defo-robinzon_kruso.pdf" title="2014 - Annual financial reports"]'); ?>
            </div>
            <?php \MyCredit\Helpers\View::render('sidebar.view.php'); ?>
        </div>
    </div>
</div>