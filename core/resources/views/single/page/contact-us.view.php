<section class="banner">
    <div class="banner-img">
        <img src="<?=MYCREDIT_ASSETS_URL.'/images/pages/about-us/top-banner.jpg'; ?>" alt="img" />
    </div>
    <div class="banner-content">
        <div class="banner-heading"><?php _e('Contact Us',MYCREDIT_TEXTDOMAIN); ?></div>
        <div class="banner-subheading"><?php _e('Lorem ipsum dolor sit amet, sea ne legimus percipit, ut vix dolorem consulatu. Ei mea tollit deserunt, ex per atomorum intellegebat, no vix habeo quidam percipit.',MYCREDIT_TEXTDOMAIN); ?></div>
    </div>
</section>
<div class="entry">
    <div class="container">
        <div class="main-container">
            <div class="content post-content">
                <?php the_content(); ?>
                <div class="contact-form-wrap">
                    <form id="contact_form">
                        <h2><?php _e('Send Us message',MYCREDIT_TEXTDOMAIN); ?></h2>
                        <input type="hidden" name="action" value="mycredit_contact" />
                        <?php wp_nonce_field('mycredit_contact'); ?>
                        <div class="cols cols--2">
                            <div class="col-left">
                                <div class="field">
                                    <input type="text" name="contact-name" id="contact-name" required="required" />
                                    <label for="contact-name"><?php _e('Name', MYCREDIT_TEXTDOMAIN); ?></label>
                                </div>
                                <div class="field">
                                    <input type="text" name="contact-subject" id="contact-subject" required="required" />
                                    <label for="contact-subject"><?php _e('Subject', MYCREDIT_TEXTDOMAIN); ?></label>
                                </div>
                                <div class="field no-margin">
                                    <input type="text" name="contact-email" id="contact-email" required="required" />
                                    <label for="contact-email"><?php _e('Email*', MYCREDIT_TEXTDOMAIN); ?></label>
                                </div>
                            </div>
                            <div class="col-right">
                                <div class="field no-margin">
                                    <textarea id="contact-message" name="contact-message" required="required"></textarea>
                                    <label for="contact-message"><?php _e('Message', MYCREDIT_TEXTDOMAIN) ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="btn-block">
                            <button class="btn btn--primary" type="submit" ><?php _e('Send', MYCREDIT_TEXTDOMAIN); ?></button>
                        </div>
                    </form>
                </div>
            </div><!-- .post-content -->
            <?php \MyCredit\Helpers\View::render('sidebar.view.php'); ?>
        </div><!-- .main-container -->
    </div><!-- .container -->
</div><!-- .entry -->