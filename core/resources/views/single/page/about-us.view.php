<section class="banner">
    <div class="banner-img">
        <img src="<?=MYCREDIT_ASSETS_URL.'/images/pages/about-us/top-banner.jpg'; ?>" alt="img" />
    </div>
    <div class="banner-content">
        <div class="banner-heading"><?php _e('Welcome to MyCredit!',MYCREDIT_TEXTDOMAIN); ?></div>
        <div class="banner-subheading"><?php _e('Lorem ipsum dolor sit amet, sea ne legimus percipit, ut vix dolorem consulatu. Ei mea tollit deserunt, ex per atomorum intellegebat, no vix habeo quidam percipit.',MYCREDIT_TEXTDOMAIN); ?></div>
    </div>
</section>
<div class="entry">
    <div class="container">
        <div class="main-container">
            <div class="content post-content">
                <?php the_content(); ?>
            </div><!-- .post-content -->
            <?php \MyCredit\Helpers\View::render('sidebar.view.php'); ?>
        </div><!-- .main-container -->
    </div><!-- .container -->
</div><!-- .entry -->