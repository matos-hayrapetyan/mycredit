<div class="entry">
    <div class="container">
        <div class="main-container">
            <div class="content post-content">
                <h1 class="h2 post-title"><?php the_title(); ?></h1>
                <div class="post-meta">
                    <div class="post-date"><?php echo date('d F, Y', strtotime(get_the_date())); ?></div>
                    <div class="excerpt"><?php the_excerpt(); ?></div>
                    <div class="featured-img"><?php the_post_thumbnail('full'); ?></div>
                </div>
                <?php the_content(); ?>
                <div class="post-comments">
                    <div class="fb-comments" data-width="100%<section class="banner">
                    <div class="banner-img">
                        <img src="<?=MYCREDIT_ASSETS_URL.'/images/pages/about-us/top-banner.jpg'; ?>" alt="img" />
                    </div>
                    <div class="banner-content">
                        <div class="banner-heading"><?php _e('Welcome to MyCredit!',MYCREDIT_TEXTDOMAIN); ?></div>
                        <div class="banner-subheading"><?php _e('Lorem ipsum dolor sit amet, sea ne legimus percipit, ut vix dolorem consulatu. Ei mea tollit deserunt, ex per atomorum intellegebat, no vix habeo quidam percipit.',MYCREDIT_TEXTDOMAIN); ?></div>
                    </div>
                    </section>
                    <div class="entry">
                        <div class="container">
                            <div class="main-container">
                                <div class="content post-content">
                                    <?php the_content(); ?>
                                </div><!-- .post-content -->
                                <?php \MyCredit\Helpers\View::render('sidebar.view.php'); ?>
                            </div><!-- .main-container -->
                        </div><!-- .container -->
                    </div><!-- .entry -->"></div>
                </div>
            </div><!-- .post-content -->
            <?php \MyCredit\Helpers\View::render('sidebar.view.php'); ?>
        </div><!-- .main-container -->
    </div><!-- .container -->
</div><!-- .entry -->