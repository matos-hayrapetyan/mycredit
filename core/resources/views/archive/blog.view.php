<?php
$posts = \MyCredit\Services\Blog::getBlogPosts(array(
    'paged' => 1,
    'posts_per_page' => 4
));
?>
<div class="entry">
    <div class="container">
        <div class="main-container">
            <div class="content">
                <h1 class="entry-heading-center"><?php _e('Blog', MYCREDIT_TEXTDOMAIN); ?></h1>
                <ul class="blog-posts" data-paged="1" data-total="<?php echo \MyCredit\Services\Blog::getBlogPostsPageCount(); ?>">
                    <?php \MyCredit\Helpers\View::render('archive/blog-posts.view.php', compact('posts')); ?>
                </ul>
            </div><!-- .content -->
            <?php \MyCredit\Helpers\View::render('sidebar.view.php'); ?>
        </div><!-- .main-container -->
    </div><!-- .container -->
</div><!-- .entry -->