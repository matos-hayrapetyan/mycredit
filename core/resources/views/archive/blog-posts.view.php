<?php
/**
 * @var $posts WP_Post[]
 */

if(!empty($posts)):
    foreach($posts as $post):
        $permalink = get_permalink($post->ID);
        ?>
        <li>
            <article>
                <div class="featured-img"><a href="<?php echo $permalink; ?>"><?php echo get_the_post_thumbnail($post->ID,'blog-thumbnail'); ?></a></div>
                <div class="meta">
                    <h2 class="blog-post-title"><a href="<?php echo $permalink; ?>"><?php echo $post->post_title; ?></a></h2>
                    <span class="post-date"><a href="<?php echo $permalink; ?>"><?php echo date('d F, Y', strtotime($post->post_date)); ?></a></span>
                    <p class="post-excerpt"><a href="<?php echo $permalink; ?>"><?php echo $post->post_excerpt; ?></a></p>
                    <div class="btn-block">
                        <a class="read-more" href="<?php echo $permalink; ?>"><?php _e('Read More', MYCREDIT_TEXTDOMAIN); ?></a>
                    </div>
                </div>
            </article>
        </li>
        <?php
    endforeach;
endif;
