<div class="widget">
    <div class="widget-title"><?php _e('Credit Calculator',MYCREDIT_TEXTDOMAIN); ?></div>
    <div class="widget-content">
        <form id="calculation-form" action="#" method="post">
            <div class="-field-wrap -field-wrap--slider">
                <div class="-field-label"><?php _e('AMOUNT (THOUSAND AMD)', MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="-field">
                    <input type="number" id="calculator-amount" name="calculator-amount" value="500" title="amount" />
                </div>
            </div>
            <div class="-field-wrap -field-wrap--slider">
                <div class="-field-label"><?php _e('DURATION', MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="-field">
                    <input type="number" name="calculator-duration" id="calculator-duration" value="24" title="duration" />
                </div>
            </div>
            <div class="-field-wrap">
                <div class="-field-label"><?php _e('PERCENT(%)', MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="-field">
                    <input type="text" name="calculator-percent" id="calculator-percent" value="22" class="-field-text -field-full-width" title="Percent" />
                </div>
            </div>
            <div class="-field-wrap">
                <div class="-field-label"><?php _e('PAYMENT METHOD', MYCREDIT_TEXTDOMAIN); ?></div>
                <div class="-field">
                    <select name="calculator-method" id="calculator-method" class="-field-text -field-full-width" title="Method" >
                        <option value="monthlyfixed"><?php _e('Monthly Fixed',MYCREDIT_TEXTDOMAIN); ?></option>
                        <option value="decreasing"><?php _e('Monthly Decreasing',MYCREDIT_TEXTDOMAIN); ?></option>
                    </select>
                </div>
            </div>
            <div class="-field-wrap text-center">
                <button type="submit" class="btn bt--primary"><span class="btn-text"><?php _e('Calculate',MYCREDIT_TEXTDOMAIN); ?></span></button>
            </div>
        </form>
    </div>
</div>