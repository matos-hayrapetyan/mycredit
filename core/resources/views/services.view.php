<?php
$servicesPosts = \MyCredit\Services\Blog::getServicePosts();
?>
<h2 class="services-lead"><?php _e('Services',MYCREDIT_TEXTDOMAIN); ?></h2>
<ul class="services">
    <?php foreach($servicesPosts as $servicesPost): ?>
        <li>
            <a href="<?=$servicesPost->getPermalink(); ?>">
                <div class="service-icon">
                    <span class="icon"><?=$servicesPost->getIcon();?></span>
                    <span class="hover-icon"><?=$servicesPost->getHoverIcon();?></span>
                </div>
                <div class="service-title"><?=$servicesPost->getPostData()->post_title; ?></div>
            </a>
        </li>
    <?php endforeach; ?>
</ul>