<?php
/**
 * Template Name: Contact Us
 */
get_header();

\MyCredit\Helpers\View::render('single/page/contact-us.view.php');

get_footer();