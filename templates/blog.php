<?php
/**
 * Template Name: Blog
 */
get_header();

\MyCredit\Helpers\View::render('archive/blog.view.php');

get_footer();
