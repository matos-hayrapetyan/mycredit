<?php
/**
 * Template Name: About Us
 */
get_header();

\MyCredit\Helpers\View::render('single/page/about-us.view.php');

get_footer();