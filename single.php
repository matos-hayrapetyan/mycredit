<?php
get_header();

if(has_tag('blog')):
    \MyCredit\Helpers\View::render('single/blog.view.php');
endif;

get_footer();